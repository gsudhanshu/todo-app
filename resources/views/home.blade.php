@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">My ToDo List</div>

                <div class="card-body">
                    <a href="{{ route('new') }}">New</a><br />
                    @forelse ($todos as $todo)
                        {{substr($todo->due_date,0,10)}} : <a href="{{ route('show', ['id' => $todo->id]) }}">{{$todo->subject}}</a> : {{$todo->description}}<br />
                    @empty
                        Hurray! No todos
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
