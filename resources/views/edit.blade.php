@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ToDo# {{ $id }}</div>

                <div class="card-body">
                    <form action="/edit/{{ $id }}" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" class="form-control" id="subject" name="subject" value="{{$todo->subject}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description">{{$todo->description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="due_date">Due Date</label>
                            <input type="date" class="form-control" id="due_date" name="due_date" value="{{substr($todo->due_date,0,10)}}">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label><br />
                            Active<input type="radio" class="form-control" id="status" name="status" value="Active" checked>
                            Done<input type="radio" class="form-control" id="status" name="status" value="Done">
                        </div>
                        <button type="submit" class="btn btn-default">Update</button>
                    </form>
                    <form action="/delete/{{ $id }}" method="post">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?');">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
