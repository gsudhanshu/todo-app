<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $todos = \DB::table('todoList')->where('user_id', Auth::id())->where('status', 'Active')->orderBy('due_date', 'asc')->get();
        return view('home', ['todos'=> $todos]);
    }

    /**
     * Add new todo
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function new()
    {
        return view('new');
    }

    /**
     * Add new todo - post
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function submit(Request $request) 
    {
        \DB::insert(
            'insert into todoList (user_id, subject, description, due_date) values (?, ?, ?, ?)',
            [Auth::id(), $request->input('subject'), $request->input('description'), $request->input('due_date')]
        );
        return redirect(route('home'));
    }

    /**
     * Show selected todo
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id) {
        $todo = \DB::table('todoList')->where('id', $id)->where('user_id', Auth::id())->first();
        return view('edit', ['todo'=> $todo, 'id'=>$id]);
    }

    /**
     * Update todo
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id, Request $request) {
        \DB::update(
            'update todoList set subject = ?, description = ?, due_date = ?, status = ? where id = ?',
            [$request->input('subject'), $request->input('description'), $request->input('due_date'), $request->input('status'), $id]
        );
        return redirect(route('home'));
    }

    /**
     * Delete todo
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function delete($id) {
        \DB::table('todoList')->where('id', $id)->delete();
        return redirect(route('home'));
    }
}
