<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/new', 'HomeController@new')->name('new');

Route::post('/new', 'HomeController@submit');

Route::get('/edit/{id}', 'HomeController@show')->name('show');

Route::post('/edit/{id}', 'HomeController@edit');

Route::post('/delete/{id}', 'HomeController@delete')->name('del');
